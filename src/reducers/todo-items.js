const createTodoItem = (action, state) => {
	const { items } = state
	const payloadItem = action.payload

	const item = items.find((item) => item.id === payloadItem.id)

	if (item === undefined) {
		payloadItem.id = new Date().getTime()
		return [...items, payloadItem]
	} else {
		const itemIndex = items.findIndex(({ id }) => id === item.id)
		return [
			...items.slice(0, itemIndex),
			payloadItem,
			...items.slice(itemIndex + 1),
		]
	}
}

const deleteTodoItem = (action, state) => {
	const { items } = state
	const itemId = action.payload
	const itemIndex = items.findIndex(({ id }) => id === itemId)

	return [...items.slice(0, itemIndex), ...items.slice(itemIndex + 1)]
}

const updateTodoItem = (action, state) => {
	const itemId = action.payload
	const item = state.items.find((item) => item.id === itemId)
	return {
		...item,
	}
}

const updateTodoList = (state, action) => {
	if (state === undefined) {
		return {
			items: [],
			loading: true,
			error: false,
			edit: null,
		}
	}

	switch (action.type) {
		case 'FETCH_TODO_REQUEST':
			return {
				items: [],
				loading: true,
				error: false,
				edit: null,
			}
		case 'FETCH_TODO_SUCCESS':
			return {
				items: action.payload,
				loading: false,
				error: false,
				edit: null,
			}
		case 'FETCH_TODO_ERROR':
			return {
				items: [],
				loading: false,
				error: true,
				edit: null,
			}
		case 'CREATE_TODO_ITEM':
			return {
				items: createTodoItem(action, state),
				loading: false,
				error: false,
				edit: null,
			}
		case 'DELETE_TODO_ITEM':
			return {
				items: deleteTodoItem(action, state),
				loading: false,
				error: false,
				edit: null,
			}
		case 'UPDATE_TODO_ITEM':
			return {
				...state,
				edit: updateTodoItem(action, state),
			}
		default:
			return state
	}
}

export default updateTodoList
