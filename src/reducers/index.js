import { combineReducers } from 'redux'
import updateTodoList from './todo-items'

export default combineReducers({
	updateTodoList,
})
