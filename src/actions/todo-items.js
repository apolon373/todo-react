const TodoIsLoading = () => {
	return {
		type: 'FETCH_TODO_REQUEST',
	}
}

const TodoRequested = (newItems) => {
	return {
		type: 'FETCH_TODO_SUCCESS',
		payload: newItems,
	}
}

const TodoHasError = (error) => {
	return {
		type: 'FETCH_TODO_ERROR',
		payload: error,
	}
}

const createTodoItem = (item) => {
	return {
		type: 'CREATE_TODO_ITEM',
		payload: item,
	}
}

const deleteTodoItem = (id) => {
	return {
		type: 'DELETE_TODO_ITEM',
		payload: id,
	}
}

const updateTodoItem = (id) => {
	return {
		type: 'UPDATE_TODO_ITEM',
		payload: id,
	}
}

const itemsFetchData = (TodoListService) => {
	return (dispatch) => {
		dispatch(TodoIsLoading())
		TodoListService.getTodos()
			.then((response) => {
				return response
			})
			.then((items) => dispatch(TodoRequested(items)))
			.catch(() => dispatch(TodoHasError(true)))
	}
}

export {
	TodoIsLoading,
	TodoHasError,
	TodoRequested,
	itemsFetchData,
	createTodoItem,
	deleteTodoItem,
	updateTodoItem,
}
