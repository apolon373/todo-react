import React from 'react'
import { Button, ListGroupItem } from 'reactstrap'
import { deleteTodoItem, updateTodoItem } from '../../actions/todo-items'
import './todo-list-item.css'
import PropTypes from 'prop-types'
import { useDispatch } from 'react-redux'

const TodoListItem = ({ item }) => {
	const { id, name, surname, date, image } = item

	const dispatch = useDispatch()

	function deleteItem(id) {
		return () => {
			dispatch(deleteTodoItem(id))
		}
	}
	function updateItem(item) {
		return () => {
			dispatch(updateTodoItem(item))
		}
	}

	return (
		<ListGroupItem>
			<div className='avatar'>
				<img src={image} alt='avatar' />
			</div>

			<div className='item-name'>{name}</div>
			<div className='item-surname'>{surname}</div>
			<div className='item-date'>{date}</div>

			<span className='primary-btns list-group-buttons'>
				<Button color='primary' onClick={updateItem(id)}>
					update
				</Button>
				<Button color='danger' onClick={deleteItem(id)}>
					delete
				</Button>
			</span>
		</ListGroupItem>
	)
}

TodoListItem.propTypes = {
	item: PropTypes.shape({
		name: PropTypes.string.isRequired,
		surname: PropTypes.string.isRequired,
		date: PropTypes.string.isRequired,
		image: PropTypes.string.isRequired,
	}),
}

export default TodoListItem
