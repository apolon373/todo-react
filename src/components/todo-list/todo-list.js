import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { ListGroup } from 'reactstrap'
import { connect } from 'react-redux'

import { itemsFetchData } from '../../actions/todo-items'
import TodoListService from '../../services/todo-list-service'

import './todo-list.css'

import TodoListItem from '../todo-list-item'
import CostumeSpinner from '../costume-spinner'
import ErrorIndicator from '../error-indicator'

const TodoList = ({ items, loading, error, fetchData }) => {
	useEffect(() => {
		fetchData()
		console.log('render todo list')
	}, [])

	if (error) {
		return <ErrorIndicator />
	}

	if (loading) {
		return <CostumeSpinner />
	}

	const renderItems = items.map((item) => (
		<TodoListItem key={item.id} item={item} />
	))
	return <ListGroup>{renderItems}</ListGroup>
}

const mapStateToProps = (state) => ({
	items: state.items,
	loading: state.loading,
	error: state.error,
})
const mapDispatchToProps = (dispatch) => ({
	fetchData: () => dispatch(itemsFetchData(new TodoListService())),
})

TodoList.propTypes = {
	items: PropTypes.array.isRequired,
	loading: PropTypes.bool.isRequired,
	error: PropTypes.bool.isRequired,
	fetchData: PropTypes.func.isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoList)
