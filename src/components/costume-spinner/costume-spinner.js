import React from 'react'
import Spinner from 'reactstrap/es/Spinner'

import './cosume-spinner.css'

const CostumeSpinner = () => {
	return (
		<div className='costume-spinner'>
			<Spinner style={{ width: '3rem', height: '3rem' }} />
			<Spinner style={{ width: '3rem', height: '3rem' }} type='grow' />
		</div>
	)
}

export default CostumeSpinner
