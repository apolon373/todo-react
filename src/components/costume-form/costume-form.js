import React, { useEffect, useState } from 'react'
import { connect, useDispatch } from 'react-redux'
import './costume-form.css'
import { createTodoItem } from '../../actions/todo-items'
import { Button, Form, FormGroup, Input } from 'reactstrap'

const defaultImage =
	'https://icon-library.net/images/upload-photo-icon/upload-photo-icon-21.jpg'

const CostumeForm = ({ edit }) => {
	const [id, setId] = useState('')
	const [name, setName] = useState('')
	const [surname, setSurname] = useState('')
	const [date, setDate] = useState('')
	const [image, setImage] = useState(defaultImage)
	const [disabled, setDisabled] = useState(true)

	const dispatch = useDispatch()

	useEffect(() => {
		if (edit !== null) {
			const { id, name, surname, date, image } = edit
			setId(id)
			setName(name)
			setSurname(surname)
			setDate(date)
			setImage(image)
		}
	}, [edit])

	useEffect(() => {
		onDisabledCheck(name, surname, date)
	}, [name, surname, date])

	function onDisabledCheck() {
		if (name.length !== 0 && surname.length !== 0 && date.length !== 0) {
			setDisabled(false)
		}
	}

	function onLabelNameChange(e) {
		setName(e.target.value)
	}

	function onLabelSurnameChange(e) {
		setSurname(e.target.value)
	}

	function setDateToState(e) {
		setDate(e.target.value)
	}

	function clearForm() {
		setId('')
		setName('')
		setSurname('')
		setDate('')
		setImage(defaultImage)
		setDisabled(true)
	}

	function onImageChange(e) {
		if (e.target.files && e.target.files[0]) {
			setImage(URL.createObjectURL(e.target.files[0]))
		}
	}

	function createTodo() {
		dispatch(
			createTodoItem({
				id,
				name,
				surname,
				date,
				image,
			})
		)

		clearForm()
	}

	return (
		<Form className='form'>
			<FormGroup className='image-upload'>
				<label htmlFor='file-input'>
					<img src={image} alt='select' className='form-avatar' />
				</label>
				<Input
					id='file-input'
					type='file'
					name='name'
					onChange={onImageChange}
				/>
			</FormGroup>

			<div className='form-inputs'>
				<FormGroup className='form-input'>
					<Input
						type='name'
						name='name'
						id='exampleName'
						placeholder='type name'
						onChange={onLabelNameChange}
						value={name}
						required
					/>
				</FormGroup>

				<FormGroup className='form-input'>
					<Input
						type='surname'
						name='surname'
						id='exampleSurname'
						placeholder='type surname'
						onChange={onLabelSurnameChange}
						value={surname}
						required
					/>
				</FormGroup>

				<FormGroup className='form-input'>
					<Input
						type='date'
						name='date'
						id='exampleDate'
						placeholder='type date'
						onChange={setDateToState}
						value={date}
						required
					/>
				</FormGroup>

				<span className='primary-btns form-buttons'>
					<Button onClick={clearForm} outline color='danger'>
						Cancel
					</Button>
					<Button
						disabled={disabled}
						onClick={createTodo}
						outline
						color='success'
					>
						Save
					</Button>
				</span>
			</div>
		</Form>
	)
}

const mapStateToProps = (state) => ({
	edit: state.edit,
})

export default connect(mapStateToProps)(CostumeForm)
