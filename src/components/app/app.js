import React from 'react'
import { Container } from 'reactstrap'
import './app.css'

import TodoList from '../todo-list'
import CostumeForm from '../costume-form'

const App = () => {
	return (
		<Container>
			<div className='head' />

			<TodoList />

			<CostumeForm />
		</Container>
	)
}

export default App
