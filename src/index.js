import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

import updateTodoList from './reducers/todo-items'

import 'bootstrap/dist/css/bootstrap.min.css'

import App from './components/app'

const store = createStore(updateTodoList, applyMiddleware(thunk))

ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root')
)
